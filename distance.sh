#!/bin/bash


# Download from here:
# https://github.com/jbarnoud/splitleafs
splitleafs --atom POPC:P -- md_PROT-MEMB-CENTERED.gro  > leaflet.ndx

cat index.ndx leaflet.ndx > index.ndx.tmp
# I changed default name of index groups

mv index.ndx.tmp index.ndx

gmx distance -f md_noPBC.xtc -s md.tpr -n index.ndx -oxyz xyz-distance.xvg -select 'com of group PROT plus com of group P_POPC_UP'

