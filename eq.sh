#!/bin/bash -l
#SBATCH --time=36:00:00
#SBATCH --job-name=B-PM
#SBATCH --partition=medium
#SBATCH --ntasks-per-node=128
#SBATCH --nodes=6
#SBATCH --account=flolicat
#SBATCH --mail-type=END
##SBATCH --mail-user=your.email@your.domain  # edit the email and uncomment to get mail

# this script runs a 256 core (2 full nodes, no hyperthreading) gromacs job, requesting 15 minutes time

module purge
module load gcc/9.3.0 openmpi/4.0.3 gromacs/2021

export OMP_NUM_THREADS=1


WINDOWs=(
#1.5
#1.6
#1.7
#1.8
#1.9
#2.0
#2.1
#2.2
#2.3
#2.4
#2.5
#2.6
#2.7
#2.8
2.9
3.0
3.1
3.2
3.3
3.4
3.5
3.6
3.7
3.8
3.9
4.0
4.1
4.2
4.3
4.4
4.5
4.6
4.7
4.8
4.9
5.0
5.1
5.2
5.3
5.4
5.5
5.6
5.7
5.8
5.9
6.0
6.1
)


for i in ${WINDOWs[@]};do
    cd $i

    gmx_mpi grompp -f ../equilibration_step1.mdp -p ../topol.top -c em.gro -o equilibration_step1.tpr -n ../index.ndx -r em.gro -maxwarn 1 
    srun gmx_mpi mdrun -deffnm equilibration_step1

    gmx_mpi grompp -f ../equilibration_step2.mdp -p ../topol.top -c equilibration_step1.gro -o equilibration_step2.tpr -n ../index.ndx -r em.gro -maxwarn 1 
    srun gmx_mpi mdrun -deffnm equilibration_step2

    gmx_mpi grompp -f ../equilibration_step3.mdp -p ../topol.top -c equilibration_step2.gro -o equilibration_step3.tpr -n ../index.ndx -r em.gro -maxwarn 1 
    srun gmx_mpi mdrun -deffnm equilibration_step3

    gmx_mpi grompp -f ../equilibration_step4.mdp -p ../topol.top -c equilibration_step3.gro -o equilibration_step4.tpr -n ../index.ndx -r em.gro -maxwarn 1 
    srun gmx_mpi mdrun -deffnm equilibration_step4

    gmx_mpi grompp -f ../equilibration_step5.mdp -p ../topol.top -c equilibration_step4.gro -o equilibration_step5.tpr -n ../index.ndx -r em.gro -maxwarn 1 
    srun gmx_mpi mdrun -deffnm equilibration_step5

    cd ..
done


rm all.gro
for i in ${WINDOWs[@]};do
    cd $i
    cat equilibration_step5.gro >> ../all.gro
    cd ..
done
