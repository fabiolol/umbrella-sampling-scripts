 #!/bin/bash

WINDOWs=(
1.5
1.6
1.7
1.8
1.9
2.0
2.1
2.2
2.3
2.4
2.5
2.6
2.7
2.8
2.9
3.0
3.1
3.2
3.3
3.4
3.5
3.6
3.7
3.8
3.9
4.0
4.1
4.2
4.3
4.4
4.5
4.6
4.7
4.8
4.9
5.0
5.1
5.2
5.3
5.4
5.5
5.6
5.7
5.8
5.9
6.0
6.1
)

module load gromacs/2021
rm all.gro
touch all.gro

for i in ${WINDOWs[@]};do
    cd $i

    gmx_mpi grompp -f ../em.mdp -c w.tpr -p ../topol.top -n ../index.ndx -t w.cpt -o time.tpr
    gmx_mpi editconf -f time.tpr -o time.gro
    cat time.gro >> ../all.gro
    rm time.gro

    cd ..
done

