#!/bin/bash -l
#SBATCH --time=36:00:00
#SBATCH --job-name=D-PM
#SBATCH --partition=medium
#SBATCH --ntasks-per-node=128
#SBATCH --nodes=6
#SBATCH --account=Project_2004692
#SBATCH --mail-type=END
##SBATCH --mail-user=your.email@your.domain  # edit the email and uncomment to get mail

# this script runs a 256 core (2 full nodes, no hyperthreading) gromacs job, requesting 15 minutes time

module purge
module load gcc/9.3.0 openmpi/4.0.3 gromacs/2021

export OMP_NUM_THREADS=1

for i in {1..10};do

	if [ ! -f w.part0*.gro ];then
		srun gmx_mpi mdrun -deffnm w -cpi w.cpt -maxh 36 -dlb yes -noappend
		sleep 1
	fi

done

sleep 10

if [ ! -f "w.part0*.gro " ]; then
  sbatch job.sh
fi

